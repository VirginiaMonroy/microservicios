const mongoose = require('mongoose');

var productSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: 'Este campo es requerido.'
    },
    categoria: {
        type: String
    },
    marca: {
        type: String
    },
    precio: {
        type: Number
    }
});

mongoose.model('Product', productSchema);